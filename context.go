package tgrouter

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"math"
)

type HandlerFunc func(*Context)

type HandlersChain []HandlerFunc

const abortIndex int8 = math.MaxInt8 / 2

type Context struct {
	User               *tgbotapi.User
	Message            *tgbotapi.Message
	CallbackQuery      *tgbotapi.CallbackQuery
	CallbackQueryData  *CallbackQueryData
	InlineQuery        *tgbotapi.InlineQuery
	ChosenInlineResult *tgbotapi.ChosenInlineResult
	hasError           error

	handlers HandlersChain
	index    int8
	keys     map[string]interface{}

	user interface{}
}

type CallbackQueryData struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func (c *Context) HasError() bool {
	return c.hasError != nil
}

func (c *Context) SetError(err error) {
	c.hasError = err
}

func (c *Context) GetUser() interface{} {
	return c.user
}

func (c *Context) Abort() {
	c.index = abortIndex
}

func (c *Context) Next() {
	c.index++
	for s := int8(len(c.handlers)); c.index < s; c.index++ {
		c.handlers[c.index](c)
	}
}

func (c *Context) IsAborted() bool {
	return c.index >= abortIndex
}

func (c *Context) Set(key string, value interface{}) {
	if c.keys == nil {
		c.keys = make(map[string]interface{})
	}
	c.keys[key] = value
}

func (c *Context) Get(key string) (value interface{}, exists bool) {
	value, exists = c.keys[key]
	return
}

func (c *Context) MustGet(key string) interface{} {
	if value, exists := c.Get(key); exists {
		return value
	}
	panic("Key \"" + key + "\" does not exist")
}
