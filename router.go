package tgrouter

import (
	"encoding/json"
	"github.com/go-telegram-bot-api/telegram-bot-api"
)

type Engine struct {
	RouterGroup
	tree Tree

	GetUser  func(*tgbotapi.User) interface{}
	SaveUser func(interface{})
}

func New() *Engine {
	engine := &Engine{
		RouterGroup: RouterGroup{
			Handlers: nil,
			step:     0,
			root:     true,
		},

		tree: Tree{},
	}

	engine.RouterGroup.engine = engine

	return engine
}

func (engine *Engine) Use(middleware ...HandlerFunc) IRoutes {
	engine.RouterGroup.Use(middleware...)
	return engine
}

func (engine *Engine) Run(bot *tgbotapi.BotAPI) error {
	// Init thread goroutine
	for stepKey, stepValue := range engine.tree {
		for phraseKey, phraseValue := range stepValue {
			for optionKey, optionValue := range phraseValue {
				if optionValue.threads >= 1 {
					optionValue.queue = make(chan *Context, 1000)
					engine.tree[stepKey][phraseKey][optionKey] = optionValue

					for i := 0; i < optionValue.threads; i++ {
						go func(thread chan *Context) {
							for {
								ctx := <-thread
								ctx.handlers[0](ctx)

								if ctx.user != nil {
									engine.SaveUser(ctx.user)
								}
							}
						}(engine.tree[stepKey][phraseKey][optionKey].queue)
					}
				}
			}
		}
	}

	// Init standard goroutine
	thread := make(chan *Context, 1000)
	go func() {
		for {
			ctx := <-thread
			ctx.handlers[0](ctx)

			if ctx.user != nil {
				engine.SaveUser(ctx.user)
			}
		}
	}()

	// Get updates
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		return err
	}

	for update := range updates {
		var (
			ctx      = &Context{}
			treeType int
			phrase   string
			options  []TreeOption
			exist    bool
		)

		// Get new messages
		if update.Message != nil {
			if update.Message.IsCommand() {
				treeType = treeTypeCommand
				phrase = update.Message.Command()
			} else {
				treeType = treeTypeMessage
				phrase = update.Message.Text
			}

			ctx.User = update.Message.From
			ctx.Message = update.Message
		} else if update.CallbackQuery != nil {
			var callbackQueryData CallbackQueryData
			treeType = treeTypeCallbackQuery

			if err := json.Unmarshal([]byte(update.CallbackQuery.Data), &callbackQueryData); err == nil {
				ctx.CallbackQueryData = &callbackQueryData
				phrase = callbackQueryData.Key
			} else {
				phrase = update.CallbackQuery.Data
			}

			ctx.CallbackQuery = update.CallbackQuery
			ctx.Message = update.CallbackQuery.Message
			ctx.User = update.CallbackQuery.From
		} else if update.InlineQuery != nil {
			treeType = treeTypeInlineQuery
			phrase = ""

			ctx.InlineQuery = update.InlineQuery
			ctx.User = update.InlineQuery.From
		} else if update.ChosenInlineResult != nil {
			treeType = treeTypeChosenInlineResult
			phrase = ""

			ctx.ChosenInlineResult = update.ChosenInlineResult
			ctx.User = update.ChosenInlineResult.From
		} else {
			continue
		}

		// Find route
		options, exist = engine.tree[treeType][phrase]
		if !exist {
			options, exist = engine.tree[treeType][""]
			if !exist {
				continue
			}
		}

		// Check user step
		for key, option := range options {
			if option.user {
				u := engine.GetUser(ctx.User).(User)
				if u.GetStep() != option.step && option.step != -1 {
					continue
				}

				ctx.user = u
			}

			ctx.handlers = option.handlers

			if option.threads >= 1 {
				engine.tree[treeType][phrase][key].queue <- ctx
			} else {
				thread <- ctx
			}

			break
		}
	}

	return nil
}
