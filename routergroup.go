package tgrouter

type IRoutes interface {
	Use(...HandlerFunc) IRoutes

	AnyMessage(...HandlerFunc) IRoutes
	Message(string, ...HandlerFunc) IRoutes
	Command(string, ...HandlerFunc) IRoutes
	CallbackQuery(string, ...HandlerFunc) IRoutes
}

type RouterGroup struct {
	Handlers HandlersChain
	step     int
	engine   *Engine
	root     bool
	user     bool
	threads  int
}

func (group *RouterGroup) Message(message string, handlers ...HandlerFunc) IRoutes {
	return group.handle(treeTypeMessage, message, handlers)
}

func (group *RouterGroup) Command(command string, handlers ...HandlerFunc) IRoutes {
	return group.handle(treeTypeCommand, command, handlers)
}

func (group *RouterGroup) CallbackQuery(data string, handlers ...HandlerFunc) IRoutes {
	return group.handle(treeTypeCallbackQuery, data, handlers)
}

func (group *RouterGroup) InlineQuery(handlers ...HandlerFunc) IRoutes {
	return group.handle(treeTypeInlineQuery, "", handlers)
}

func (group *RouterGroup) ChosenInlineResult(handlers ...HandlerFunc) IRoutes {
	return group.handle(treeTypeChosenInlineResult, "", handlers)
}

func (group *RouterGroup) AnyMessage(handlers ...HandlerFunc) IRoutes {
	return group.handle(treeTypeMessage, "", handlers)
}

func (group *RouterGroup) Use(middleware ...HandlerFunc) IRoutes {
	group.Handlers = append(group.Handlers, middleware...)
	return group.returnObj()
}

func (group *RouterGroup) SetThread(count int) *RouterGroup {
	group.threads = count
	return group
}

func (group *RouterGroup) AnyStep(handlers ...HandlerFunc) *RouterGroup {
	return &RouterGroup{
		Handlers: group.combineHandlers(handlers),
		step:     -1,
		engine:   group.engine,
		user:     true,
		threads:  -1,
	}
}

func (group *RouterGroup) Step(step int, handlers ...HandlerFunc) *RouterGroup {
	return &RouterGroup{
		Handlers: group.combineHandlers(handlers),
		step:     step,
		engine:   group.engine,
		user:     true,
		threads:  -1,
	}
}

func (group *RouterGroup) WithUser(handlers ...HandlerFunc) *RouterGroup {
	return &RouterGroup{
		Handlers: group.combineHandlers(handlers),
		step:     -1,
		engine:   group.engine,
		user:     true,
		threads:  -1,
	}
}

func (group *RouterGroup) WithoutUser(handlers ...HandlerFunc) *RouterGroup {
	return &RouterGroup{
		Handlers: group.combineHandlers(handlers),
		step:     -1,
		engine:   group.engine,
		user:     false,
		threads:  -1,
	}
}

func (group *RouterGroup) returnEng() *Engine {
	if group.root {
		return group.engine
	}
	return group.engine
}

func (group *RouterGroup) returnObj() IRoutes {
	if group.root {
		return group.engine
	}
	return group
}

func (group *RouterGroup) combineHandlers(handlers HandlersChain) HandlersChain {
	finalSize := len(group.Handlers) + len(handlers)
	if finalSize >= int(abortIndex) {
		panic("too many handlers")
	}

	mergedHandlers := make(HandlersChain, finalSize)
	copy(mergedHandlers, group.Handlers)
	copy(mergedHandlers[len(group.Handlers):], handlers)
	return mergedHandlers
}

func (group *RouterGroup) handle(treeType int, phrase string, handlers HandlersChain) IRoutes {
	e := group.returnEng()

	if e.tree[treeType] == nil {
		e.tree[treeType] = map[string][]TreeOption{}
	}

	option := TreeOption{
		handlers: handlers,
		step:     group.step,
		threads:  group.threads,
		user:     group.user,
	}

	e.tree[treeType][phrase] = append(e.tree[treeType][phrase], option)

	return group.returnObj()
}
